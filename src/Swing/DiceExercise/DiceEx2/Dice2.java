package Swing.DiceExercise.DiceEx2;

import javax.swing.*;

class Dice2 extends JLabel {
    private final String ROUTE_IMG = "src/Swing/DiceExercise/img/";
    private int diceRoll;

    public Dice2() {
        diceRoll = (int) (Math.ceil(Math.random() * 6));
        ImageIcon myIMG;
        myIMG = new ImageIcon(ROUTE_IMG + diceRoll + ".png");

        setText("Has sacado un " + diceRoll);
        setIcon(myIMG);
        setHorizontalAlignment(CENTER);
    }

}
