package Swing.DiceExercise.DiceEx1;

import javax.swing.*;

class Dice {
    private final String ROUTE_IMG = "src/Swing/DiceExercise/img/";
    private final String[] DICE = {
            ROUTE_IMG + "1.png",
            ROUTE_IMG + "2.png",
            ROUTE_IMG + "3.png",
            ROUTE_IMG + "4.png",
            ROUTE_IMG + "5.png",
            ROUTE_IMG + "6.png"
    };

    public JLabel roll() {
        int diceRoll = (int) (Math.ceil(Math.random() * 6));
        ImageIcon myIMG;
        JLabel myJLabel = new JLabel();
        switch (diceRoll) {
            case 1:
                myIMG = new ImageIcon(DICE[0]);
                myJLabel = new JLabel("Has sacado un 1", myIMG, JLabel.CENTER);
                break;
            case 2:
                myIMG = new ImageIcon(DICE[1]);
                myJLabel = new JLabel("Has sacado un 2", myIMG, JLabel.CENTER);
                break;
            case 3:
                myIMG = new ImageIcon(DICE[2]);
                myJLabel = new JLabel("Has sacado un 3", myIMG, JLabel.CENTER);
                break;
            case 4:
                myIMG = new ImageIcon(DICE[3]);
                myJLabel = new JLabel("Has sacado un 4", myIMG, JLabel.CENTER);
                break;
            case 5:
                myIMG = new ImageIcon(DICE[4]);
                myJLabel = new JLabel("Has sacado un 5", myIMG, JLabel.CENTER);
                break;
            case 6:
                myIMG = new ImageIcon(DICE[5]);
                myJLabel = new JLabel("Has sacado un 6", myIMG, JLabel.CENTER);
                break;
        }

        return myJLabel;

    }
}
