package Swing.JList;

import javax.swing.*;
import java.awt.*;

public class List extends JFrame {
    public static void main(String[] args) {
        
        List myList = new List();
        myList.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myList.setVisible(true);
    }

    public List(){
        FlowLayout myLayout = new FlowLayout();
        setLayout(myLayout);

        String [] countries = {"España", "Francia", "Colombia", "México"};
        JList<String> myList = new JList<>(countries);
        add(myList);
        
        setSize(300, 300);
        setLocationRelativeTo(null);
    }
}
