package Swing.JEditorPane;

import javax.swing.*;
import java.awt.*;

public class MyEditorPane extends JFrame {
    public static void main(String[] args) {
        MyEditorPane myFakePage = new MyEditorPane("Fake page");

        myFakePage.setVisible(true);
        myFakePage.setLocationRelativeTo(null);
    }

    public MyEditorPane(String title) throws HeadlessException {
        super(title);
        JEditorPane editorPane = new JEditorPane("text/html",
                "<html>"+ "<h1 style='color: blue'>"+"Esto es una prueba"+"</h1>"+"</html>");
        editorPane.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(editorPane);

        setDefaultCloseOperation(MyEditorPane.EXIT_ON_CLOSE);
        getContentPane().add(scrollPane);
        setBounds(500,500,500,500);

    }
}
