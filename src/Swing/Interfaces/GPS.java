package Swing.Interfaces;

public interface GPS {
    public void getCoordinates();
    default public void defaultMethod(){
        System.out.println("Este método se instancia por defecto!\n" +
                "Pero los métodos default son compatibles a partir de la versión 8 de Java");
    }
}
