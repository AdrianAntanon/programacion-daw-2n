package Swing.Interfaces;

public class Main {
    public static void main(String[] args) {
        Smartphone motorola = new Smartphone();
        motorola.getCoordinates();
        motorola.startRadio();
        motorola.stopRadio();
        motorola.defaultMethod();

    }
}
