package Swing.Layout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class MyBoxLayout extends JFrame {
    public static void main(String[] args) {
//        BoxLayout apila els components un sobre l’altre o els col·loca seguits, segons la vostra elecció.

        MyBoxLayout myBoxLayout =  new MyBoxLayout("Ejemplo de BoxLayout");

        myBoxLayout.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myBoxLayout.setVisible(true);

    }

    public MyBoxLayout(String title){
        super(title);
        setSize(500, 500);

        Container cp = getContentPane();
        LayoutManager boxLayout = new BoxLayout(cp, BoxLayout.PAGE_AXIS);

        setLayout(boxLayout);

        JButton a = new JButton("Uno");
        JButton b = new JButton("Dos");
        JButton c = new JButton("Tres");
        JButton d = new JButton("Cuatro");
        JButton e = new JButton("Cinco");

        add(a);
        //Esta es la forma de crear un padding entre los botones
        add(Box.createRigidArea(new Dimension(2, 10)));

        add(b);
        add(Box.createRigidArea(new Dimension(2, 10)));

        add(c);
        add(Box.createRigidArea(new Dimension(2, 10)));

        add(d);
        add(Box.createRigidArea(new Dimension(2, 10)));

        add(e);
        add(Box.createRigidArea(new Dimension(2, 10)));

        setLocationRelativeTo(null);
    }
}
