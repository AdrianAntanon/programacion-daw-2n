package Swing.Layout;

import javax.swing.*;
import java.awt.*;

public class MyBorderLayout extends JFrame {
    public static void main(String[] args) {

//        BorderLayout Divideix el contenidor en 5 seccions. Sud, nord, est, oest i centre.
//        S’acostuma a fer servir quan la finestra per contenir una barra de botons.
//        El únic canvi respecte a altres contenidors es que a la hora de afegir un component haurem d’especificar en quina secció ho volem fer.

        MyBorderLayout myBorderLayout = new MyBorderLayout("Ejemplo de BorderLayout");

        myBorderLayout.setDefaultCloseOperation(MyBorderLayout.EXIT_ON_CLOSE);
        myBorderLayout.setVisible(true);
    }

    public MyBorderLayout(String title){
        super(title);
        setSize(360, 120);

        setLayout(new BorderLayout());

        JButton a = new JButton("Norte");
        JButton b = new JButton("Sur");
        JButton c = new JButton("Este");
        JButton d = new JButton("Oeste");
        JButton e = new JButton("Centro");

        add(a, BorderLayout.NORTH);
        add(b, BorderLayout.SOUTH);
        add(c, BorderLayout.EAST);
        add(d, BorderLayout.WEST);
        add(e, BorderLayout.CENTER);
    }
}
