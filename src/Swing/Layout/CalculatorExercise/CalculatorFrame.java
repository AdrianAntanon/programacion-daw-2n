package Swing.Layout.CalculatorExercise;

import javax.swing.*;

class CalculatorFrame extends JFrame {
    public CalculatorFrame(String title) {
        super(title);
        setSize(350, 500);

        CalculatorPane myCalculatorPane = new CalculatorPane();
        add(myCalculatorPane);

        setLocationRelativeTo(null);
    }
}
