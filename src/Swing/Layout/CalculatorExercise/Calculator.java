package Swing.Layout.CalculatorExercise;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Calculator {
    public static void main(String[] args) {
//        Con este try catch lo que hago es registrar la Font que usaré en el ejercicio
        try {
            GraphicsEnvironment ge =
                    GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("src/Fonts/Calculator.ttf")));
        } catch (IOException | FontFormatException e) {
            //Handle exception
            System.out.println(e.getMessage());
        }

        CalculatorFrame calc = new CalculatorFrame("My own calculator");

        calc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        calc.setVisible(true);
    }
}

