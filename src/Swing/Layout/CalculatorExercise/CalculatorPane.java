package Swing.Layout.CalculatorExercise;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

class CalculatorPane extends JPanel {

    public CalculatorPane() {
        setLayout(new BorderLayout());

        String[] results = {"111", "222", "333"};

        JPanel text = new JPanel();
        text.setLayout(new GridLayout(3, 1, 1, 1));
        text.setBorder(new EmptyBorder(10, 15, 10, 15));

        for (String result : results) {
            addDisplay(result, text);
        }

        add(text, BorderLayout.NORTH);

        JPanel numbers = new JPanel();
        numbers.setLayout(new GridLayout(4, 4, 5, 5));
        numbers.setBorder(new EmptyBorder(10, 15, 20, 15));

        String[] numbersAndOperators = {"1", "2", "3", "+", "4", "5", "6", "-", "7", "8", "9", "=", "0"};

        for (String numberOrOperator : numbersAndOperators) {
            addKeyboard(numberOrOperator, numbers);
        }

        add(numbers, BorderLayout.CENTER);
    }

    private void addDisplay(String text, JPanel textPanel) {
        Display screen = new Display(text);

        textPanel.add(screen);
    }

    private void addKeyboard(String textButton, JPanel numbers) {
        Keyboard button = new Keyboard(textButton);

        if (textButton.equalsIgnoreCase("0")) {
            Keyboard disabledButton = new Keyboard("");
            disabledButton.setVisible(false);
            numbers.add(disabledButton);
        }

        numbers.add(button);

    }
}
