package Swing.Layout.CalculatorExercise;

import javax.swing.*;
import java.awt.*;

class Display extends JTextField {
    public Display(String title) {
        super(title);
        setHorizontalAlignment(JTextField.RIGHT);
        setFont(new Font("Calculator", Font.PLAIN, 45));
    }
}
