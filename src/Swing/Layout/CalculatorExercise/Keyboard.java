package Swing.Layout.CalculatorExercise;

import javax.swing.*;
import java.awt.*;

class Keyboard extends JButton {
    public Keyboard(String title) {
        super(title);
        setFont(new Font("Calculator", Font.BOLD, 25));
    }
}
