package Swing.Layout;

import javax.swing.*;

public class MyFlowLayout extends JFrame {
    public static void main(String[] args) {
//        FlowLayout el gestor més simple de tots, organitza els elements de forma horitzontal
//        sempre i quan tingui amplada suficient per mostrar tots els components en la mateixa línia. En cas contrari fa un salt i continua.

        MyFlowLayout flowLayout = new MyFlowLayout("Ejemplo de FlowLayout");

        flowLayout.setDefaultCloseOperation(MyFlowLayout.EXIT_ON_CLOSE);
        flowLayout.setVisible(true);
    }

    public MyFlowLayout(String title){
        super(title);
        setSize(300, 250);

        java.awt.FlowLayout myLayout = new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 25, 15);
        setLayout(myLayout);

        JButton a = new JButton("Primer botón");
        JButton b = new JButton("Segundo botón");
        JButton c = new JButton("Tercer botón");
        JButton d = new JButton("Cuarto botón");
        JButton e = new JButton("Quinto botón");
        JButton f = new JButton("Sexto botón");

        add(a);
        add(b);
        add(c);
        add(d);
        add(e);
        add(f);

        setLocationRelativeTo(null); //Esto hace que se centre automáticamente, independientemente del tamaño de la pantalla

    }
}
