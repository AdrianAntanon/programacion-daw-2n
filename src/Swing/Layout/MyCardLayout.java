package Swing.Layout;

import javax.swing.*;
import java.awt.*;

public class MyCardLayout extends JFrame {
    public static void main(String[] args) {
//      CardLayout ens permet crear múltiples diàlegs i pibotar entre ells, es similar a JTabbedPane  que implementa una funcionalitat similar pero amb ‘pestanyes’

        MyCardLayout myCardLayout = new MyCardLayout("Ejemplo de CardLayout");

        myCardLayout.setDefaultCloseOperation(MyCardLayout.EXIT_ON_CLOSE);
        myCardLayout.setVisible(true);
    }

    public MyCardLayout(String title){
        super(title);
        setSize(660, 320);

        JPanel mainPanel = new JPanel();
        JPanel buttonPanel = new JPanel();

        JPanel firstPanel = new JPanel();
        JPanel secondPanel = new JPanel();
        JPanel thirdPanel = new JPanel();
        JPanel fourthPanel = new JPanel();
        JPanel fifthPanel = new JPanel();

        firstPanel.setBackground(Color.GREEN);
        secondPanel.setBackground(Color.RED);
        thirdPanel.setBackground(Color.WHITE);
        fourthPanel.setBackground(Color.CYAN);
        fifthPanel.setBackground(Color.PINK);

        JLabel firstLabel = new JLabel("Content of Card 1 is visible now!");
        JLabel secondLabel = new JLabel("Content of Card 2 is visible now!");
        JLabel thirdLabel = new JLabel("Content of Card 3 is visible now!");
        JLabel fourthLabel = new JLabel("Content of Card 4 is visible now!");
        JLabel fifthLabel = new JLabel("Content of Card 5 is visible now!");

        JButton firstButton = new JButton("Display Card 1");
        JButton secondButton = new JButton("Display Card 2");
        JButton thirdButton = new JButton("Display Card 3");
        JButton fourthButton = new JButton("Display Card 4");
        JButton fifthButton = new JButton("Display Card 5");

        CardLayout cardLayout = new CardLayout();
        mainPanel.setLayout(cardLayout);

        firstPanel.add(firstLabel);
        secondPanel.add(secondLabel);
        thirdPanel.add(thirdLabel);
        fourthPanel.add(fourthLabel);
        fifthPanel.add(fifthLabel);

        mainPanel.add(firstPanel, "Link 1");
        mainPanel.add(secondPanel, "Link 2");
        mainPanel.add(thirdPanel, "Link 3");
        mainPanel.add(fourthPanel, "Link 4");
        mainPanel.add(fifthPanel, "Link 5");

//        Añado eventos a los botones, para asegurarme de que cambie el name tiene que ser igual al constraint asignado al mainPanel

        firstButton.addActionListener(e -> cardLayout.show(mainPanel, "Link 1"));
        secondButton.addActionListener(e -> cardLayout.show(mainPanel, "Link 2"));
        thirdButton.addActionListener(e -> cardLayout.show(mainPanel, "Link 3"));
        fourthButton.addActionListener(e -> cardLayout.show(mainPanel, "Link 4"));
        fifthButton.addActionListener(e -> cardLayout.show(mainPanel, "Link 5"));

        buttonPanel.add(firstButton);
        buttonPanel.add(secondButton);
        buttonPanel.add(thirdButton);
        buttonPanel.add(fourthButton);
        buttonPanel.add(fifthButton);

        add(mainPanel,  BorderLayout.NORTH);
        add(buttonPanel, BorderLayout.SOUTH);
    }
}
