package Swing.JSplitPane;

import javax.swing.*;
import java.awt.*;

public class MyJSplitPane extends JFrame {
    public static void main(String[] args) {
        MyJSplitPane myJSplitPane = new MyJSplitPane("Ejemplo de SplitPane");

        myJSplitPane.setBounds(500,500,500,500);
        myJSplitPane.setVisible(true);
        myJSplitPane.setLocationRelativeTo(null);
        myJSplitPane.setDefaultCloseOperation(MyJSplitPane.EXIT_ON_CLOSE);
    }
    public MyJSplitPane(String title) throws HeadlessException {
        super(title);

        JPanel panelFirst = new JPanel();
        JPanel panelSecond = new JPanel();

        JTextArea textAreaFirst = new JTextArea(10, 10);
        JTextArea textAreaSecond = new JTextArea(10, 10);

        textAreaFirst.setText("this is the first text area");
        textAreaSecond.setText("this is the second text area");

        panelFirst.add(textAreaFirst);
        panelSecond.add(textAreaSecond);

//        JSplitPane jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, panelFirst, panelSecond); //Alineamiento vertical
        JSplitPane jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panelFirst, panelSecond); // Alineamiento horizontal

        add(jSplitPane);
    }
}
