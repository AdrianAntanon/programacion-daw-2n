package Swing.JFrame;

import javax.swing.*;
import java.awt.*;

class MyFirstJFrame extends JFrame {
    public MyFirstJFrame(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(600, 350, 600, 300);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
//        setResizable(false);
    }
}
