package Swing.JFrame;

import javax.swing.*;
import java.awt.*;

class MyFirstTF extends JTextField {
    public MyFirstTF(String text) {
        super(text);
        setBackground(Color.RED);
        setForeground(Color.WHITE);
        setFont(new Font("Serif", Font.ITALIC, 20));
        setEditable(false); // Para poder modificarlo
        setHorizontalAlignment(JTextField.CENTER);
    }
}
