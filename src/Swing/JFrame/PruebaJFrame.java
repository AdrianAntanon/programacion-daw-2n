package Swing.JFrame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class PruebaJFrame {
    public static void main(String[] args) throws IOException {
        String icon = "src/Swing/PildorasInformaticas/Interfaces/img/boots-dr-martens.jpg";
        ImageIcon myIMG = new ImageIcon(ImageIO.read(new File(icon)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));

        JLabel myJLabel = new JLabel("Esto es una prueba", myIMG, JLabel.CENTER);

        MyFirstTF firstText = new MyFirstTF("Funciona");

        MyFirstJFrame firstJFrame = new MyFirstJFrame("Hola mundo!");
        firstJFrame.add(firstText, BorderLayout.SOUTH);
        firstJFrame.add(myJLabel);
//        firstJFrame.pack();
// Recuerda que el pack decide la medida de la ventana dependiendo del tamaño de los componentes, por lo que se pasa por los huevos el setBounds o setSize

        firstJFrame.setVisible(true);
    }
}


