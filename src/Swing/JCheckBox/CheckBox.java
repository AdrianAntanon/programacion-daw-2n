package Swing.JCheckBox;

import javax.swing.*;
import java.awt.*;

public class CheckBox extends JFrame {
    public static void main(String[] args) {
        CheckBox myCheckBox = new CheckBox();
        myCheckBox.setDefaultCloseOperation(CheckBox.EXIT_ON_CLOSE);
        myCheckBox.setVisible(true);
    }

    public CheckBox(){
        FlowLayout myLayout = new FlowLayout();
        setLayout(myLayout);

        JCheckBox optionOne = new JCheckBox("Opción 1");
        add(optionOne);
        JCheckBox optionTwo = new JCheckBox("Opción 2");

        add(optionTwo);

        JCheckBox option1 = new JCheckBox("Opción LOL");
        JCheckBox option2 = new JCheckBox("Opción 3");
        JCheckBox option3 = new JCheckBox("Opción 4");
        JCheckBox option4 = new JCheckBox("Opción 5");
        JCheckBox option5 = new JCheckBox("Opción 6");
        JCheckBox option6 = new JCheckBox("Opción 7");

        add(option1);
        add(option2);
        add(option3);
        add(option4);
        add(option5);
        add(option6);

        setSize(300, 300);
        setLocationRelativeTo(null);
    }
}
