package Swing.JTabbedPane;

import javax.swing.*;
import java.awt.*;

public class MyTabbedPane extends JFrame {
    public static void main(String[] args) {
        MyTabbedPane tabbedPane = new MyTabbedPane();

        tabbedPane.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tabbedPane.setVisible(true);
    }

    public MyTabbedPane(){
        getContentPane().setLayout(new BorderLayout());
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

        JPanel pSeat = new JPanel();
        pSeat.add(new Label("Seat"));
        pSeat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Seat", pSeat);

        JPanel pSkoda = new JPanel();
        pSkoda.add(new Label("Skoda"));
        pSkoda.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Skoda", pSkoda);

        JPanel pFiat = new JPanel();
        pFiat.add(new Label("Fiat"));
        pFiat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Fiat", pFiat);

        getContentPane().add(tabbedPane);
        setSize(200, 200);
    }
}
