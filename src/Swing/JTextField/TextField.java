package Swing.JTextField;

import javax.swing.*;
import java.awt.*;

public class TextField extends JFrame {
    public static void main(String[] args) {
        TextField textField = new TextField("Frame con un JTextField", "Prueba de texto");
//        Paso por parámetro el nombre del frame y el contenido por defecto del textField
        textField.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        textField.setVisible(true);

    }

    public TextField(String title, String text){
        super(title);
        FlowLayout myLayout = new FlowLayout();
        setLayout(myLayout);

        JTextField myOwnText = new JTextField(20);
        myOwnText.setText(text);
        myOwnText.setHorizontalAlignment(SwingConstants.CENTER); // Centramos el texto de manera horizontal, aquí empezaremos a escribir desde el medio
        add(myOwnText);
        setSize(500, 500);
        setLocationRelativeTo(null);
    }
}
