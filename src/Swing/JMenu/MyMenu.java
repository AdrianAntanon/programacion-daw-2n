package Swing.JMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyMenu extends JFrame {

    public static void main(String[] args) {
        MyMenu newMenu = new MyMenu("Menu");
        newMenu.setVisible(true);
        newMenu.setBounds(500, 500, 500, 500);
        newMenu.setDefaultCloseOperation(MyMenu.EXIT_ON_CLOSE);
        newMenu.setLocationRelativeTo(null);
    }

    private MyMenu(String title) throws HeadlessException {
        super(title);

        class MenuListenerExample implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                JMenuItem selection = (JMenuItem) e.getSource();
                String action = selection.getActionCommand();
                if (action.compareTo("Info")==0){
                    JOptionPane.showMessageDialog(MyMenu.this, "This program shows you the functions of the menu", "Information", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }

        MenuListenerExample menuListener = new MenuListenerExample();

        JMenuBar menuBar = new JMenuBar();

        JMenu file = new JMenu("File");
        JMenuItem newFile = new JMenuItem("New");
        newFile.addActionListener(menuListener);
        file.add(newFile);

        JMenuItem open = new JMenuItem("Open");
        open.addActionListener(menuListener);
        file.add(open);

        JMenuItem info = new JMenuItem("Info");
        info.addActionListener(menuListener);
        file.add(info);

        JMenuItem close = new JMenuItem("Close");
        close.addActionListener(menuListener);
        file.add(close);

        menuBar.add(file);

        JMenu edit = new JMenu("Edit");
        JMenuItem cut = new JMenuItem("Cut");
        newFile.addActionListener(menuListener);
        edit.add(cut);
        menuBar.add(edit);

        setJMenuBar(menuBar);


    }
}
