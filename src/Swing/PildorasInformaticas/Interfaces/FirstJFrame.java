package Swing.PildorasInformaticas.Interfaces;

import javax.swing.*;
import java.awt.*;

public class FirstJFrame {
    public static void main(String[] args) {

        /*Swing.JFrame myWindow = new Swing.JFrame();

        myWindow.setSize(600, 300);
        myWindow.setDefaultCloseOperation(Swing.JFrame.EXIT_ON_CLOSE);
        myWindow.setLocation(600, 350);
        myWindow.setVisible(true);
*/
        MyJFrame myWindow = new MyJFrame();
    }
}

class MyJFrame extends JFrame{

    private String icon = "src/Swing/PildorasInformaticas/Interfaces/img/boots-dr-martens.jpg";

    public MyJFrame(){
//        setSize(600, 350);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Depende de los 4 valores que le pasemos se dejará de ejecutar una vez cerremos el Swing.JFrame o no.
//        setLocation(600, 350);
        setBounds(600, 350, 450, 450); // es la combinación de setSize y setLocation
        setResizable(false); // true se puede redimensionar, false NO
//        setExtendedState(Frame.MAXIMIZED_BOTH); // Obligamos al Swing.JFrame a que ocupe el máximo de la pantalla
        setLocationRelativeTo(null); // si le pasamos null el Swing.JFrame se centra automáticamente
        setTitle("Venta de prueba"); // Le damos un título al Swing.JFrame

        Toolkit mySystem = Toolkit.getDefaultToolkit();
        // Nos hará falta un objeto de tipo toolkit para acceder al método getImage()
        Image myIcon = mySystem.getImage(icon);
        //para asignar un icono para la cabecera de Swing.JFrame nos hace falta una imagen de tipo Image, le pasamos por parámetro un string con la ruta

        setIconImage(myIcon);

        setVisible(true);
    }

}
