package Swing.PildorasInformaticas.Events.KeyExamples;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyEventExample implements KeyListener {
    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println("Has tecleado una tecla");
    }

    @Override
    public void keyPressed(KeyEvent e) {

        /*if (e.getKeyCode()==KeyEvent.VK_J){
            System.out.println("Has pulsado la tecla: " + e.getKeyChar());
        }*/

        System.out.println("Has pulsado la tecla " + KeyEvent.getKeyText(e.getKeyCode()));
    }

    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("Has soltado una tecla");
    }
}
