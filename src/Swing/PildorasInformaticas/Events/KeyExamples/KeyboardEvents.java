package Swing.PildorasInformaticas.Events.KeyExamples;

import javax.swing.*;

public class KeyboardEvents {
    public static void main(String[] args) {
        JFrameKeyboard myJFrameKeyEvent = new JFrameKeyboard("PIldoras Informáticas EVENTO DE TECLADO");

        myJFrameKeyEvent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myJFrameKeyEvent.setVisible(true);

        myJFrameKeyEvent.addKeyListener(new KeyEventExample());
    }
}
