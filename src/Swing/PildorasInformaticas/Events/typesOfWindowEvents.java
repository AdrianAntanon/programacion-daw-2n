package Swing.PildorasInformaticas.Events;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

class typesOfWindowEvents implements WindowListener {

    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println("Ventana abierta");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("Cerrando ventana");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("Ventana cerrada");
    }

    @Override
    public void windowIconified(WindowEvent e) {

        System.out.println("Ventana minimizada");

    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        System.out.println("Ventana restaurada después de minimizar");
    }

    @Override
    public void windowActivated(WindowEvent e) {
        System.out.println("Ventana en primer plano");
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        System.out.println("Ventana en segundo plano");
    }
}
