package Swing.PildorasInformaticas.Events;

import javax.swing.*;

public class WindowsEvents {
    public static void main(String[] args) {

//        WindowEventFrame windowEventImplements = new WindowEventFrame("Evento usando la interfaz WindowListener");
//        windowEventImplements.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        windowEventImplements.addWindowListener(new typesOfWindowEvents());
//
//        WindowEventFrame windowEventAdapter = new WindowEventFrame("Evento usando una clase adaptadora (WindowAdapter)");
//        windowEventAdapter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        windowEventAdapter.addWindowListener(new WindowEventsAdapter());

        WindowEventFrame windowState = new WindowEventFrame("Evento usando la interfaz WindowStateListener");
        windowState.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        windowState.addWindowStateListener(new WindowEventStateListener());




    }
}

