package Swing.PildorasInformaticas.Events;

import javax.swing.*;

class WindowEventFrame extends JFrame {
    public WindowEventFrame(String title) {
        super(title);
        setSize(500, 500);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
