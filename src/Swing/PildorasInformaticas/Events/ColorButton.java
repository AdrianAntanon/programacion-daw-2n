package Swing.PildorasInformaticas.Events;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColorButton {
    public static void main(String[] args) {

        FrameColorButtons eventExample = new FrameColorButtons();

        eventExample.setVisible(true);
        eventExample.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    }
}

/*
* Dos formas de implementar la interface Action Listener
* La primera sería que lo implemente el propio PaneColorButtons
* y la forma de llevarlo a cabo es toda la parte comentada
* La segunda es crear una clase interna que se encargará de ello
*
*
* */

class FrameColorButtons extends JFrame{
    public FrameColorButtons(){
        setTitle("Botones y eventos");

        setSize(600, 600);
        setLocationRelativeTo(null);

        PaneColorButtons myPanel = new PaneColorButtons();

        add(myPanel);

    }
}

class PaneColorButtons extends JPanel /*implements ActionListener*/ {
    JButton blueButton = new JButton("Azul");
    JButton redButton = new JButton("Rojo");
    JButton greenButton = new JButton("Verde");
    public PaneColorButtons(){
        add(blueButton);
        add(redButton);
        add(greenButton);

        BackgroundColor blue = new BackgroundColor(Color.BLUE);
        BackgroundColor red = new BackgroundColor(Color.RED);
        BackgroundColor green = new BackgroundColor(Color.GREEN);

        blueButton.addActionListener(blue);
        redButton.addActionListener(red);
        greenButton.addActionListener(green);

//        blueButton.addActionListener(this);
//        redButton.addActionListener(this);
//        greenButton.addActionListener(this);

    }

    /*@Override
    public void actionPerformed(ActionEvent e) {

        Object selectedButton = e.getSource();

        if (selectedButton == blueButton) setBackground(Color.BLUE);
        else if (selectedButton == redButton) setBackground(Color.RED);
        else setBackground(Color.GREEN);

    }*/

    private class BackgroundColor implements ActionListener{
        private final Color BACKGROUND_COLOR;

        public BackgroundColor(Color backgroundColor){
            BACKGROUND_COLOR = backgroundColor;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setBackground(BACKGROUND_COLOR);
        }
    }
}
