package Swing.PildorasInformaticas.Events;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class WindowEventsAdapter extends WindowAdapter {

    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("Saliendo de la APP \nQue pase un buen día!");
    }

    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println("Buenos días, bienvenido a la APP");
    }
}
