package Swing.PildorasInformaticas.Events;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

class WindowEventStateListener implements WindowStateListener {

    @Override
    public void windowStateChanged(WindowEvent e) {
//        System.out.println(e.getNewState());

        if (e.getNewState() == JFrame.MAXIMIZED_BOTH) System.out.println("La ventana se ha maximizado");
        else if (e.getNewState() == JFrame.ICONIFIED) System.out.println("La ventana se ha minimizado");

    }
}
