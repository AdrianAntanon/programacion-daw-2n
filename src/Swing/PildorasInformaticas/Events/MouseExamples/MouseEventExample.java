package Swing.PildorasInformaticas.Events.MouseExamples;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/*public class MouseEventExample implements MouseListener {
    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("Has dejado de hacer click 1");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("Click largo 2");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("Has soltado 3");

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("Has entrado 4");

    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("Has salido 5");

    }
}*/

/*public class MouseEventExample extends MouseAdapter {
    @Override
    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
        System.out.println(e.getX() + " eje x, " +
                e.getY() + " eje Y");

        if (e.getModifiersEx() == MouseEvent.BUTTON1_DOWN_MASK) System.out.println("Has pulsado el boton izquierdo");
        else if (e.getModifiersEx() == MouseEvent.BUTTON3_DOWN_MASK) System.out.println("Has pulsado el boton derecho");
        else System.out.println("Has pulsado la rueda"); //La rueda es BUTTON2_DOWN_MASK
    }
}*/

class MouseEventExample implements MouseMotionListener{

    @Override
    public void mouseDragged(MouseEvent e) {
        System.out.println("Estás arrastrando el mouse");
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        System.out.println("Estás moviendo el mouse");
    }
}