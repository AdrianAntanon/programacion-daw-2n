package Swing.PildorasInformaticas.Events.MouseExamples;

import javax.swing.*;

public class MouseEvents {
    public static void main(String[] args) {
        JFrameMouse myMouseEvent = new JFrameMouse("Eventos de ratón PILDORAS INFORMÁTICAS");

        myMouseEvent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myMouseEvent.setVisible(true);

//        myMouseEvent.addMouseListener(new MouseEventExample());
        myMouseEvent.addMouseMotionListener(new MouseEventExample());
    }
}
