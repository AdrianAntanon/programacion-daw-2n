package Swing.JOptionPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FirstJOptionPane extends JFrame{
    public static void main(String[] args) {
        FirstJOptionPane firstJOptionPane = new FirstJOptionPane("Hi");
    }
    public FirstJOptionPane(String title) throws HeadlessException {
        super(title);

        JButton myButton = new JButton("Open window");
        FlowLayout myFlowLayout = new FlowLayout();
        setLayout(myFlowLayout);

        myButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int result = JOptionPane.showOptionDialog(
                        rootPane,
                        "Kill'em all?",
                        "Metallica",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null,
                        new Object[]{"Yes", "No"},
                        "Yes");
                if (result==1) System.out.println("So don't you like the first album, maybe you prefer Ride The Lightning");
                else System.out.println("It's the first album of Metallica");
            }
        });

        add(myButton);
        setBounds(500, 500, 500, 500);
        setVisible(true);
        setLocationRelativeTo(null);
    }
}
