package Swing.JTree;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

public class MyJTree extends JFrame {

    public static void main(String[] args) {
        MyJTree myTree = new MyJTree();

        myTree.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myTree.setVisible(true);
    }

    public MyJTree(){
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        DefaultMutableTreeNode topic = new DefaultMutableTreeNode("Java");
        DefaultMutableTreeNode thread;
        DefaultMutableTreeNode section;

        thread = new DefaultMutableTreeNode("Excepciones");
        topic.add(thread);

        section = new DefaultMutableTreeNode("Sección 1 - El concepto");
        thread.add(section);
        section = new DefaultMutableTreeNode("Sección 2 - La sintaxis");
        thread.add(section);
        section = new DefaultMutableTreeNode("Sección 3 - Práctica");
        thread.add(section);


        thread = new DefaultMutableTreeNode("Swing");
        topic.add(thread);

        section = new DefaultMutableTreeNode("Sección 1 - GUI's");
        thread.add(section);
        section = new DefaultMutableTreeNode("Sección 2 - Ejemplo de componentes");
        thread.add(section);
        section = new DefaultMutableTreeNode("Sección 3 - Ejemplo de eventos");
        thread.add(section);


        JTree myJTree = new JTree(topic);

        JScrollPane treeView = new JScrollPane(myJTree);
        cp.add(treeView);

        setSize(300, 300);
        setLocationRelativeTo(null);
    }
}
