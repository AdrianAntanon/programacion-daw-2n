package Swing.JButtonAndImageIcon;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ExampleJButtonAndImageIcon {
    public static void main(String[] args) throws IOException {
        String icon = "src/Swing/PildorasInformaticas/Interfaces/img/boots-dr-martens.jpg";

        MyJFrame ownJFrame = new MyJFrame("Uso de JBUtton y ImageIcon");

//        Esto es lo que usas para utilizar img en los botones y labels
        ImageIcon ownImageIcon = new ImageIcon(ImageIO.read(new File(icon)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));


        MyJLabel ownJLabel = new MyJLabel("Ejemplo de botas", ownImageIcon, JLabel.CENTER);

        JPanel panel = new JPanel();
        JButton button = new JButton("My button", ownImageIcon);
        panel.add(button);

        ownJFrame.add(ownJLabel, BorderLayout.SOUTH);
        ownJFrame.add(panel);
//        ownJFrame.pack();
        ownJFrame.setVisible(true);
    }
}

class MyJFrame extends JFrame {
    public MyJFrame(String title) {
        super(title);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(600, 350, 600, 500);
        setLocationRelativeTo(null);
    }

}

class MyJLabel extends JLabel{
    private String title;
    private ImageIcon myIMG;
    private int position;

    public MyJLabel(String title, ImageIcon myIMG, int position){
        super(title, myIMG, position);

        this.title = title;
        this.myIMG = myIMG;
        this.position = position;


    }
}
