package Swing.ExerciseTabbedPaneAndJTable;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class TableTabsPanels extends JFrame {
    public static void main(String[] args) throws IOException {
        TableTabsPanels tabsPanels = new TableTabsPanels("Mi taller");

        tabsPanels.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tabsPanels.setVisible(true);

    }

    public TableTabsPanels(String title) throws IOException {
        super(title);

        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

        String routeIMG = "src/Swing/ExerciseTabbedPaneAndJTable/img/";
        ImageIcon seatIcon = new ImageIcon(ImageIO.read(new File(routeIMG + "seat.jpg")).getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        ImageIcon skodaIcon = new ImageIcon(ImageIO.read(new File(routeIMG + "skoda.png")).getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        ImageIcon fiatIcon = new ImageIcon(ImageIO.read(new File(routeIMG + "fiat.png")).getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));


        JPanel pSeat = new JPanel();
        pSeat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Seat", seatIcon, pSeat);

        JPanel pSkoda = new JPanel();
        pSkoda.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Skoda", skodaIcon, pSkoda);

        JPanel pFiat = new JPanel();
        pFiat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Fiat", fiatIcon, pFiat);

        Object[][] seatCollection = carCollection(1);
        Object[][] fiatCollection = carCollection(2);
        Object[][] skodaCollection = carCollection(3);

        String[] columnName = {"Modelo", "Marca", "Matrícula", "Precio", "Cilindrada", "Año"};


        JTable seatTable = new JTable(seatCollection, columnName);
        JTable skodaTable = new JTable(skodaCollection, columnName);
        JTable fiatTable = new JTable(fiatCollection, columnName);

        Font font = new Font("Helvetica Neue", Font.BOLD, 16);

        seatTable.setFont(font);
        skodaTable.setFont(font);
        fiatTable.setFont(font);

        JScrollPane seatView = new JScrollPane(seatTable);
        JScrollPane skodaView = new JScrollPane(skodaTable);
        JScrollPane fiatView = new JScrollPane(fiatTable);

        pSeat.add(seatView);
        pSkoda.add(skodaView);
        pFiat.add(fiatView);

        cp.add(tabbedPane);

        setSize(700, 400);
        setLocationRelativeTo(null);
    }

    public Object[][] carCollection(int selection) {

        Object[][] seatCollection = {
                {"Seat", "León", "1232BCD", "10000", "1500", "2000"},
                {"Seat", "Ateca", "1523JKP", "10000", "1500", "2000"},
                {"Seat", "Ibiza", "6666YWY", "10000", "1500", "2000"},
                {"Seat", "Alhambra", "9040AAA", "10000", "1500", "2000"},
                {"Seat", "Arona", "1298KPO", "10000", "1500", "2000"},
                {"Seat", "Mii", "5893KKK", "10000", "1500", "2000"},
                {"Seat", "Tarraco", "1975LOL", "10000", "1500", "2000"},
                {"Seat", "Leon", "2020DEP", "10000", "1500", "2000"},
                {"Seat", "León", "1232BCD", "10000", "1500", "2000"},
                {"Seat", "Ateca", "1523JKP", "10000", "1500", "2000"},
                {"Seat", "Ibiza", "6666YWY", "10000", "1500", "2000"},
                {"Seat", "Alhambra", "9040AAA", "10000", "1500", "2000"},
                {"Seat", "Arona", "1298KPO", "10000", "1500", "2000"},
                {"Seat", "Mii", "5893KKK", "10000", "1500", "2000"},
                {"Seat", "Tarraco", "1975LOL", "10000", "1500", "2000"},
                {"Seat", "Leon", "2020DEP", "10000", "1500", "2000"},
                {"Seat", "León", "1232BCD", "10000", "1500", "2000"},
                {"Seat", "Ateca", "1523JKP", "10000", "1500", "2000"},
                {"Seat", "Ibiza", "6666YWY", "10000", "1500", "2000"},
                {"Seat", "Alhambra", "9040AAA", "10000", "1500", "2000"},
                {"Seat", "Arona", "1298KPO", "10000", "1500", "2000"},
                {"Seat", "Mii", "5893KKK", "10000", "1500", "2000"},
                {"Seat", "Tarraco", "1975LOL", "10000", "1500", "2000"},
                {"Seat", "Leon", "2020DEP", "10000", "1500", "2000"},
                {"Seat", "León", "1232BCD", "10000", "1500", "2000"},
                {"Seat", "Ateca", "1523JKP", "10000", "1500", "2000"},
                {"Seat", "Ibiza", "6666YWY", "10000", "1500", "2000"},
                {"Seat", "Alhambra", "9040AAA", "10000", "1500", "2000"},
                {"Seat", "Arona", "1298KPO", "10000", "1500", "2000"},
                {"Seat", "Mii", "5893KKK", "10000", "1500", "2000"},
                {"Seat", "Tarraco", "1975LOL", "10000", "1500", "2000"},
                {"Seat", "Leon", "2020DEP", "10000", "1500", "2000"},
                {"Seat", "León", "1232BCD", "10000", "1500", "2000"},
                {"Seat", "Ateca", "1523JKP", "10000", "1500", "2000"},
                {"Seat", "Ibiza", "6666YWY", "10000", "1500", "2000"},
                {"Seat", "Alhambra", "9040AAA", "10000", "1500", "2000"},
                {"Seat", "Arona", "1298KPO", "10000", "1500", "2000"},
                {"Seat", "Mii", "5893KKK", "10000", "1500", "2000"},
                {"Seat", "Tarraco", "1975LOL", "10000", "1500", "2000"},
                {"Seat", "Leon", "2020DEP", "10000", "1500", "2000"},
                {"Seat", "Paprika", "123BA", "10000", "1500", "2000"}
        };
        Object[][] skodaCollection = {
                {"Skoda", "Octavia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Karmiq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Fabia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Superb", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Kodiaq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Scala", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Citigo", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Octavia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Karmiq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Fabia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Superb", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Kodiaq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Scala", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Citigo", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Octavia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Karmiq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Fabia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Superb", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Kodiaq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Scala", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Citigo", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Octavia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Karmiq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Fabia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Superb", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Kodiaq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Scala", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Citigo", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Octavia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Karmiq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Fabia", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Superb", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Kodiaq", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Scala", "2020DEP", "15000", "1600", "2005"},
                {"Skoda", "Citigo", "2020DEP", "15000", "1600", "2005"},
        };
        Object[][] fiatCollection = {
                {"Fiat", "500", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Tipo", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Panda", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500C", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500X", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500L", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Doblò Panorama", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Tipo", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Panda", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500C", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500X", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500L", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Doblò Panorama", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Tipo", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Panda", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500C", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500X", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500L", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Doblò Panorama", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Tipo", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Panda", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500C", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500X", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500L", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Doblò Panorama", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Tipo", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Panda", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500C", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500X", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "500L", "2020DEP", "12000", "1300", "2008"},
                {"Fiat", "Doblò Panorama", "2020DEP", "12000", "1300", "2008"},
        };
        switch (selection) {
            case 1:
                return seatCollection;
            case 2:
                return fiatCollection;
            default:
                return skodaCollection;
        }
    }
}
