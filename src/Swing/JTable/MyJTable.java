package Swing.JTable;

import javax.swing.*;
import java.awt.*;

public class MyJTable extends JFrame {

    public static void main(String[] args) {
        MyJTable myTable = new MyJTable("Lista de coches");

        myTable.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myTable.setVisible(true);
    }

    public MyJTable(String title){
        super(title);
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);


        JPanel pSeat = new JPanel();
        pSeat.add(new Label("Seat"));
        pSeat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Seat", pSeat);

        JPanel pSkoda = new JPanel();
        pSkoda.add(new Label("Skoda"));
        pSkoda.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Skoda", pSkoda);

        JPanel pFiat = new JPanel();
        pFiat.add(new Label("Fiat"));
        pFiat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Fiat", pFiat);

        getContentPane().add(tabbedPane);

        final String[] columnName = {"Sección", "Práctica", "aula"};
        Object [][] data = {
                {"1", "Introducción a Eclipse", "209"},
                {"2", "Clases y objetos", "208"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},
                {"1", "Introducción a Eclipse", "209"},

        };

        JTable myOwnTable = new JTable(data, columnName);

        JScrollPane tableView = new JScrollPane(myOwnTable);
        cp.add(tableView);

        setSize(300, 300);
        setLocationRelativeTo(null);
    }
}
