package Swing.JToolbar;

import javax.swing.*;
import java.awt.*;

public class Toolbar {

    public static void main(String[] args) {

        new Toolbar();

    }
    public Toolbar() {
        JFrame mainWindow = new JFrame("test JToolbar");
        JTextArea mainComponent = new JTextArea(25, 80);

        JToolBar toolBar = getToolBar();

        mainWindow.getContentPane().add(mainComponent);
        mainWindow.getContentPane().add(toolBar, BorderLayout.NORTH);
        mainWindow.pack();

        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private JToolBar getToolBar() {
        JToolBar buttonsBar = new JToolBar();

        String ROUTE_IMG = "src/Swing/DiceExercise/img/";

        ImageIcon icon = new ImageIcon(ROUTE_IMG +"1.png");
        buttonsBar.add(new JButton("Taller", icon));

        icon = new ImageIcon(ROUTE_IMG +"3.png");
        buttonsBar.add(new JButton("Personal", icon));

        icon = new ImageIcon(ROUTE_IMG +"6.png");
        buttonsBar.add(new JButton("Facturación", icon));

        return buttonsBar;
    }
}
