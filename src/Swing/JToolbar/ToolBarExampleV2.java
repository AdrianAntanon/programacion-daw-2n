package Swing.JToolbar;

import javax.swing.*;
import java.awt.*;

public class ToolBarExampleV2 extends JFrame {
    private final JLabel firstLabel;
    private final JLabel secondLabel;
    private final JComboBox comboBox;

    public static void main(String[] args) {
        ToolBarExampleV2 toolBarExampleV2 = new ToolBarExampleV2("Esto es una prueba");

        toolBarExampleV2.setBounds(500, 500, 500, 500);
        toolBarExampleV2.setVisible(true);
        toolBarExampleV2.setDefaultCloseOperation(ToolBarExampleV2.EXIT_ON_CLOSE);
    }

    public ToolBarExampleV2(String title) throws HeadlessException {
        super(title);

        firstLabel = new JLabel("Nada seleccionado");
        secondLabel = new JLabel("Nada seleccionado");

        setLayout(new BorderLayout());

        JToolBar toolBar = new JToolBar();

        JPanel panel = new JPanel();

        comboBox = new JComboBox(new String[]{"Item 1", "Item 2", "Item 3"});

//        comboBox.addActionListener(this);

        comboBox.addActionListener(e -> secondLabel.setText(comboBox.getSelectedItem() + ""));

        JButton firstButton = new JButton("Botón 1");
        JButton secondButton = new JButton("Botón 2");

//        firstButton.addActionListener(this);
//        secondButton.addActionListener(this);

        firstButton.addActionListener(e -> {
            JPanel yourPanel = new JPanel(new FlowLayout());
            yourPanel.add(new JButton("Second button"));
            yourPanel.setBackground(Color.RED);
            yourPanel.setBorder(
                    BorderFactory.createTitledBorder(
                            BorderFactory.createEtchedBorder(),"Your first Panel")
            );
            add(yourPanel);
        });

        secondButton.addActionListener(e -> {
            firstLabel.setText(e.getActionCommand() + " adios");
            secondLabel.setText(comboBox.getSelectedItem() + " hola");
        });

        panel.add(firstButton);
        panel.add(secondButton);

        panel.add(comboBox);

        toolBar.add(panel);

        JPanel anotherPanel = new JPanel();

        anotherPanel.add(firstLabel);
        anotherPanel.add(secondLabel);

        add(toolBar, BorderLayout.NORTH);
        add(anotherPanel);
    }

}
