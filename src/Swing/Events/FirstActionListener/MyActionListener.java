package Swing.Events.FirstActionListener;

import javax.swing.*;
import java.awt.*;

//      Las líneas comentadas son porque inicialmente funcionaba implementando ActionListener,
//      pero voy más rápido añadiendo un addActionLister al bottón, le paso por parámetro un new Action... y me genera una inner class automáticamente

public class MyActionListener extends JFrame /*implements ActionListener*/ {
//    private JButton myButton;
    public static void main(String[] args) {
        MyActionListener myTest = new MyActionListener();

        myTest.setDefaultCloseOperation(MyActionListener.EXIT_ON_CLOSE);
        myTest.setVisible(true);
    }

    public MyActionListener(){
        setLayout(new FlowLayout());
        JButton myButton = new JButton("Test events click");

        myButton.addActionListener(e -> {
            System.out.println("It works");
            myButton.setText("Keep working");
            add(myButton);
        });

        add(myButton);
        setBounds(600, 350, 300, 300);
        setLocationRelativeTo(null);
    }

//    @Override
//    public void actionPerformed(ActionEvent actionEvent) {
//        System.out.println("It works");
//        myButton.setText("Keep working");
//        add(myButton);
//    }
}
