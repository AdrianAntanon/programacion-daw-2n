package Swing.Events.EventsListSelection;

import javax.swing.*;

public class EventListSelectionEX {
    public static void main(String[] args) {
        JFrameListSelection myNewList = new JFrameListSelection("ListSelection Event example");

        myNewList.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myNewList.setVisible(true);
    }
}
