package Swing.Events.EventsListSelection;

import javax.swing.*;
import java.awt.*;

class JFrameListSelection extends JFrame {
    public JFrameListSelection(String title) throws HeadlessException {
        super(title);
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);


        JPanel pSeat = new JPanel();
        pSeat.add(new Label("ID"));
        pSeat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Seat", pSeat);

        JPanel pSkoda = new JPanel();
        pSkoda.add(new Label("Nom"));
        pSkoda.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Skoda", pSkoda);

        JPanel pFiat = new JPanel();
        pFiat.add(new Label("Salari"));
        pFiat.setLayout(new GridLayout(1, 1));
        tabbedPane.addTab("Fiat", pFiat);

        getContentPane().add(tabbedPane);

        String[] columnName = {"ID", "Nom", "Aula"};
        String [][] data = {
                {"101", "Amit", "206"},
                {"102", "Rafa", "208"},
                {"101", "Marta", "209"},
        };

        JTable myOwnTable = new JTable(data, columnName);

        JScrollPane tableView = new JScrollPane(myOwnTable);
        cp.add(tableView);

        ListSelectionModel listSelectionModel = myOwnTable.getSelectionModel();
        listSelectionModel.addListSelectionListener(e -> {
            int row = myOwnTable.getSelectedRow();
            int column = myOwnTable.getSelectedColumn();
            String selectedItem = (String) myOwnTable.getValueAt(row, column);
            JOptionPane.showMessageDialog(null, "Fila: " + (row+1) + "   Columna: " + (column+1) + "\nValor seleccionado: " + selectedItem);
        });

        setSize(300, 300);
        setLocationRelativeTo(null);
    }
}
