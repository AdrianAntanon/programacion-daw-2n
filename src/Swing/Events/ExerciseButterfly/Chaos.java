package Swing.Events.ExerciseButterfly;

import javax.swing.*;
import java.awt.*;

public class Chaos extends JFrame {
    protected JLabel myJlabel;

    public static void main(String[] args) {
        Chaos myEventTest = new Chaos();

        myEventTest.setDefaultCloseOperation(Chaos.EXIT_ON_CLOSE);
        myEventTest.setVisible(true);
    }

    private Chaos(){
        setLayout(new FlowLayout());

        myJlabel = new JLabel("HURRICANE!!!");
        myJlabel.setVisible(false);

        JButton myButton = new JButton("BUTTERFLY!!");

        add(myButton);
        add(myJlabel);

        MyEventListener myEventListener = new MyEventListener();
        myButton.addActionListener(myEventListener);
        setSize(500, 500);
        setVisible(true);
    }
}
