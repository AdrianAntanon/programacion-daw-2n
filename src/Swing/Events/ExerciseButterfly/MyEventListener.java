package Swing.Events.ExerciseButterfly;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyEventListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        System.out.println("Hola, qué tal?");
        JButton test = (JButton) actionEvent.getSource();
        Chaos chaos = (Chaos) SwingUtilities.getRoot(test);

//        Esta línea sustituye a un if() y else ya que cada vez que sea activado lo que hace es cambiar de true a false con el !
//        Es decir setVisible() recibe lo contrario de como está, ya que al ser booleano es true o false !
        chaos.myJlabel.setVisible(!chaos.myJlabel.isVisible());

    }
}
