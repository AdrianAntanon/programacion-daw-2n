package Swing.Events.EventsKeyListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

public class Spaceship extends JFrame {
    private int positionX = 0;
    private int positionY = 0;


    public static void main(String[] args) throws IOException {

        Spaceship myFirstSpaceship = new Spaceship("Rocinante");

        myFirstSpaceship.setLocationRelativeTo(null);
        myFirstSpaceship.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFirstSpaceship.setVisible(true);
    }

    public Spaceship(String title) throws IOException {
        super(title);
        setBounds(500, 500, 500, 500);
        setFocusable(true);

        final String ROUTE_IMG = "src/Swing/Events/img/spaceship.png";
        ImageIcon img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG)).getScaledInstance(30, 30, Image.SCALE_SMOOTH));

        JLabel myJLabel = new JLabel();

        myJLabel.setIcon(img);
        myJLabel.setHorizontalAlignment(JLabel.CENTER);


        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

                switch (keyEvent.getKeyCode()){
                    case KeyEvent.VK_UP:
                        myJLabel.setLocation(positionX, positionY-=5);
                        break;
                    case KeyEvent.VK_DOWN:
                        myJLabel.setLocation(positionX, positionY+=5);
                        break;
                    case KeyEvent.VK_RIGHT:
                        myJLabel.setLocation(positionX+=5, positionY);
                        break;
                    case KeyEvent.VK_LEFT:
                        myJLabel.setLocation(positionX-=5, positionY);
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });

        add(myJLabel);

    }


}
