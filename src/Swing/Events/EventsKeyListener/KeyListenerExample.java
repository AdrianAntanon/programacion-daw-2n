package Swing.Events.EventsKeyListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class KeyListenerExample {
    public static void main(String[] args) {
        JFrame myJframe = new JFrame("Eventos de teclado");

        myJframe.setSize(new Dimension(600, 600));
        myJframe.setFocusable(true);
        myJframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myJframe.setLayout(new BorderLayout());

        JTextField text = new JTextField("Usa las flechas y verás el KeyListener en la consola");
        myJframe.add(text, BorderLayout.NORTH);

        myJframe.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            public void keyPressed(KeyEvent keyEvent) {

                if (keyEvent.getKeyCode() == KeyEvent.VK_UP){
                    System.out.println("Up");
                }

                if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN){
                    System.out.println("Down");
                }

                if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT){
                    System.out.println("Right");
                }

                if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT){
                    System.out.println("Left");
                }

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }

        });

        myJframe.setVisible(true);
    }
}
