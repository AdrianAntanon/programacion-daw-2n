package Swing.Events.EventsItemListAndComboBox;

import javax.swing.*;
import java.awt.*;

public class ItemList extends JFrame /*implements ItemListener*/ {

    private final String [] titles = {
            "Selecciona un formato",
            "1",
            "2",
            "3"
    };

    private JComboBox box = new JComboBox(titles);
//    private JLabel myJlabel;

// Igual que en el ejercicio de MyActionListener, voy más rápido con una lambda que me genera una inner class implementando la interfaz ItemListener
    public static void main(String[] args) {
        ItemList test = new ItemList();

        test.setDefaultCloseOperation(ItemList.EXIT_ON_CLOSE);
        test.setVisible(true);
    }

    public ItemList(){
        setLayout(new BorderLayout());

        add(box, BorderLayout.NORTH);
        JLabel myJlabel = new JLabel();
        add(myJlabel);

        String [] answers = {
                "Esto es un 1",
                "Esto es un 2",
                "Esto es un 3"
        };

        box.addItemListener(e -> {
            int position = box.getSelectedIndex();

            if (position > 0){
                myJlabel.setText(answers[position-1]);
            }else {
                myJlabel.setText("");
            }
        });

        setBounds(600, 350, 300, 300);
        setLocationRelativeTo(null);
    }

//    @Override
//    public void itemStateChanged(ItemEvent itemEvent) {
//        int position = box.getSelectedIndex();
//
//        if (position > 0){
//
//            myJlabel.setText(answers[position-1]);
//        }else {
//            myJlabel.setText("");
//        }
//    }
}
