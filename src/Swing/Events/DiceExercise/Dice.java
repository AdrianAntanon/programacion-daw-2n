package Swing.Events.DiceExercise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Dice extends JFrame implements ActionListener {
    private final String ROUTE_IMG = "src/Swing/Events/DiceExercise/img/";
    private int diceRoll;
    private final JButton myJButton;

    public static void main(String[] args) {
        Dice firstDice = new Dice();

        firstDice.setDefaultCloseOperation(Dice.EXIT_ON_CLOSE);
        firstDice.setVisible(true);

    }

    public Dice() {
        diceRoll = (int) (Math.ceil(Math.random() * 6));
        ImageIcon myIMG;
        myIMG = new ImageIcon(ROUTE_IMG+diceRoll+".png");

        setLayout(new FlowLayout());
        myJButton = new JButton("Hola",myIMG);
        myJButton.addActionListener(this);
        add(myJButton);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(600, 350, 300, 300);

        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        diceRoll = (int) (Math.ceil(Math.random() * 6));
        ImageIcon myIMG;
        myIMG = new ImageIcon(ROUTE_IMG+diceRoll+".png");

        myJButton.setIcon(myIMG);
    }
}
