package Swing.finalPractise;

import javax.swing.*;
import java.awt.*;

public class BillingPane extends JPanel {
    public BillingPane() {
        setLayout(new BorderLayout());

        String[] columnName = {"ID", "Fecha", "Producto","Precio","Cliente"};
        String [] carModels = {"León", "Ateca", "Ibiza", "Alhambra", "Arona", "Mii", "Tarraco"};
        int date = 10, price = 5000;
        String [][] data = new String[7][5];
        for (int index = 0; index < data.length; index++) {
            for (int subIndex = 0; subIndex < data[index].length; subIndex++) {
                switch (subIndex){
                    case 0:
                        data[index][subIndex] = "00"+(index+1);
                        break;
                    case 1:
                        data[index][subIndex] = (date+=1)+"/11/2020";
                        break;
                    case 2:
                        data[index][subIndex] = "Seat "+carModels[index];
                        break;
                    case 3:
                        data[index][subIndex] =(price += 1000) + "";
                        break;
                    case 4:
                        data[index][subIndex] = "Adrián Antañón";
                        break;
                }
            }
        }

        JTable myOwnTable = new JTable(data, columnName);

        myOwnTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        myOwnTable.setFillsViewportHeight(true);

        ListSelectionModel listSelectionModel = myOwnTable.getSelectionModel();
        listSelectionModel.addListSelectionListener(e -> {
            int row = myOwnTable.getSelectedRow();
            int column = myOwnTable.getSelectedColumn();
            String selectedItem = (String) myOwnTable.getValueAt(row, column);
            JOptionPane.showMessageDialog(null, "Se ha seleccionado fila: " + (row+1) + " y columna: " + (column+1) +
                    "\n El valor de la celda es: " + selectedItem);
        });

        JScrollPane tableView = new JScrollPane(myOwnTable);
        add(tableView);

    }
}
