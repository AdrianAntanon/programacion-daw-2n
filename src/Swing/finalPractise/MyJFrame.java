package Swing.finalPractise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;


class MyJFrame extends JFrame {

    public MyJFrame(String title) throws HeadlessException, IOException {
        super(title);

        setJMenuBar(new MyJMenuBar());

        MyJToolBar toolBar = new MyJToolBar();
        getContentPane().add(toolBar, BorderLayout.NORTH);
        pack();

        JPanel mainPanel = new JPanel();
        CardLayout cardLayout = new CardLayout();
        mainPanel.setLayout(cardLayout);

        mainPanel.add(new WorkshopPane(), "WorkshopPane");
        mainPanel.add(new HumanResourcesPane(), "HumanResourcesPane");
        mainPanel.add(new BillingPane(), "BillingPane");

        MyJToolBar.workshop.addActionListener(actionEvent -> {
            AddOrDeleteToolBar.deletePersonal.setEnabled(false);
            AddOrDeleteToolBar.addPersonal.setEnabled(false);
            cardLayout.show(mainPanel, "WorkshopPane");
        });

        MyJToolBar.humanResources.addActionListener(actionEvent -> {
            AddOrDeleteToolBar.deletePersonal.setEnabled(true);
            AddOrDeleteToolBar.addPersonal.setEnabled(true);
            cardLayout.show(mainPanel, "HumanResourcesPane");
        });

        MyJToolBar.billing.addActionListener(actionEvent -> {
            AddOrDeleteToolBar.deletePersonal.setEnabled(false);
            AddOrDeleteToolBar.addPersonal.setEnabled(false);
            cardLayout.show(mainPanel, "BillingPane");
        });

        getContentPane().add(mainPanel);

        setBounds(800,800,800,800);
        setLocationRelativeTo(null);
    }

}
