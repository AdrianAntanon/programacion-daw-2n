package Swing.finalPractise;

import java.io.IOException;

public class MainERP {
    protected static MyJFrame myERP;

    public static void main(String[] args) throws IOException {
        myERP = new MyJFrame("Mi taller");

        myERP.setVisible(true);
        myERP.setDefaultCloseOperation(MyJFrame.EXIT_ON_CLOSE);
    }
}
