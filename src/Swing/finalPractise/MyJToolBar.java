package Swing.finalPractise;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

class MyJToolBar extends JToolBar {
    static JButton workshop;
    static JButton humanResources;
    static JButton billing;

    public MyJToolBar() throws IOException {
        String ROUTE_IMG = "src/Swing/finalPractise/img/";
        String[] imgCategory = {"carWorkshop", "humanResources", "billing"};
        String extensionFile = ".png";
        ImageIcon img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG + imgCategory[0]+ extensionFile)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));

        workshop = new JButton("Taller", img);
        add(workshop);

        img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG + imgCategory[1]+ extensionFile)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));

        humanResources = new JButton("Personal", img);
        add(humanResources);

        img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG + imgCategory[2]+ extensionFile)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));
        billing = new JButton("Facturación", img);
        add(billing);

        AddOrDeleteToolBar addOrDeleteToolBar = new AddOrDeleteToolBar();


        add(addOrDeleteToolBar);


    }
}
