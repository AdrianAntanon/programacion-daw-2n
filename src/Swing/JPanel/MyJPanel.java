package Swing.JPanel;

import javax.swing.*;
import java.awt.*;

public class MyJPanel extends JFrame {

    public static void main(String[] args) {
        MyJPanel myPanel = new MyJPanel("Mi panel");

        myPanel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myPanel.setVisible(true);
    }

    public MyJPanel(String title){
        super(title);
        setLayout(new FlowLayout());
        JPanel myPanel = new JPanel(new FlowLayout());

        myPanel.add(new JButton("First button"));
        myPanel.add(new JLabel("JLabel"));

        myPanel.setBorder(
                BorderFactory.createTitledBorder(
                        BorderFactory.createEtchedBorder(),"My first Panel")
        );

        JPanel yourPanel = new JPanel(new FlowLayout());
        yourPanel.add(new JButton("Second button"));
        yourPanel.setBackground(Color.RED);
        yourPanel.setBorder(
                BorderFactory.createTitledBorder(
                        BorderFactory.createEtchedBorder(),"My second Panel")
        );

        add(myPanel);
        add(yourPanel);

        setSize(400, 300);
        setLocationRelativeTo(null);
    }
}
