package Exceptions.firstExercises;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class ExceptionsExercise {


    File fileToModify = new File("example.txt");
    Scanner sc = new Scanner(System.in);


    public static void main(String[] args) {
        ExceptionsExercise program = new ExceptionsExercise();
        program.start();
    }

    public void start(){
        System.out.println("Bienvenido al modificador de archivos.\n" +
                "Seleccione la opción que desea, por favor");

        int scape = 4;
        int selection = 0;

        String newLine = "";

        do {
            try{

                System.out.println("1) Leer fichero\n" +
                        "2) Introducir datos en el fichero\n" +
                        "3) Guardar fichero\n" +
                        "4) Salir del programa ");
                checkInt();
                selection = sc.nextInt();

                switch (selection){
                    case 1:
                        readFile();
                        break;
                    case 2:
                        newLine = introductionData();
                        break;
                    case 3:
                        saveData(newLine);
                        break;
                    case 4:
                        goodbye();
                        break;
                    default:
                        System.out.println("Opción no disponible en fase beta");
                }
            }catch (Exception e){
                System.out.println("Ha surgido el siguiente problema: " + e);
            }
        }while (selection != scape);
    }


    public void checkInt(){
        while (!sc.hasNextInt()){
            System.out.println("Número no permitido, vuelve a introducirlo, por favor");
            sc.next();
        }
    }

    public void readFile(){
        try{
            Scanner readingFile = new Scanner(fileToModify);
            while (readingFile.hasNextLine()){
                String line = readingFile.nextLine();
                System.out.println(line);
            }
        }catch (Exception e){
            System.out.println("Error inesperado " + e);
        }
    }

    public String introductionData(){
        try{
            System.out.println("Introduce el texto que quieras, para acabar dale a intro");
            sc.nextLine();
            String newLine = sc.nextLine();

            return newLine;

        }catch (Exception e){
            System.out.println("Error inesperado " + e);

            return null;
        }
    }

    public void saveData(String newLine){
        try {
            FileWriter fw = new FileWriter(fileToModify.getAbsoluteFile(), true);
            fw.write("\n");
            fw.write(newLine);
            fw.flush();

        }catch (Exception e){
            System.out.println("Error inesperado " + e);
        }
    }

    public void goodbye(){
        System.out.println("Saliendo del programa, que pase un buen día\n" +
                "AUTODESTRUYENDO PROGRAMA 3... 2... 1...");
        sc.close();
    }
}
