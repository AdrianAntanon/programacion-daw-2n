package Exceptions.firstExercises.RepasoExamen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex1 {

    private final File fileToModify = new File("example.txt");
    private final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Ex1 program = new Ex1();

        program.start();
    }

//El do while(true) es un polling infinito, dentro le metemos el try catch para que me recoja las excepciones y me siga preguntando, así no finaliza.
    public void start(){
        System.out.println("Bienvenido al modificador de archivos.\n" +
                "Seleccione la opción que desea, por favor");

        int scape = 4;
        int selection = 0;

        StringBuilder newLine = new StringBuilder();

        do {
            try{
                menuOptions();
                checkInt();
                selection = sc.nextInt();

                switch (selection){
                    case 1:
                        readFile();
                        break;
                    case 2:
                        newLine.append(introductionData());
                        break;
                    case 3:
                        saveData(newLine.toString());
                        newLine = new StringBuilder();
                        break;
                    case 4:
                        goodbye();
                        break;
                    case 5:
                        deleteContent();
                        break;
                    default:
                        System.out.println("Opción no disponible en fase beta");
                }
            }catch (Exception e){
                System.out.println("Ha surgido el siguiente problema: " + e);
            }
        }while (selection != scape);
    }

//    Menú
    public void menuOptions(){
        System.out.println("1) Leer fichero\n" +
                "2) Introducir datos en el fichero\n" +
                "3) Guardar fichero\n" +
                "4) Salir del programa \n"+
                "5) Borrar contenido");
    }

//  Comprobación que es un integer el número introducido
    public void checkInt(){
        while (!sc.hasNextInt()){
            System.out.println("Número no permitido, vuelve a introducirlo, por favor");
            sc.next();
        }
    }

//    Leemos el contenido del fichero pasándole un archivo a Scanner
    public void readFile(){
        try{
            Scanner readingFile = new Scanner(fileToModify);
            while (readingFile.hasNextLine()){
                String line = readingFile.nextLine();
                System.out.println(line);
            }
        }catch (Exception e){
            System.out.println("Error inesperado " + e);
        }
    }

//    Guardamos texto para más tarde guardarlo en el archivo txt
    public String introductionData(){
        try{
            System.out.println("Introduce el texto que quieras, para acabar dale a intro");
            sc.nextLine();
            String newLine = sc.nextLine()+"\n";
            System.out.println("Datos en stage");
            return newLine;

        }catch (Exception e){
            System.out.println("Error inesperado " + e);

            return null;
        }
    }

//    Añadimos nuestro texto guardado al fichero, solo lo realizamos si tenemos algo guardado
    public void saveData(String newLine){
        try {
            if (!newLine.isEmpty()){
                FileWriter fw = new FileWriter(fileToModify.getAbsoluteFile(), true);
                fw.write("\n");
                fw.write(newLine);
                fw.flush();
                System.out.println("Datos guardados");
            }else {
                System.out.println("No hay nada que guardar");
            }

        }catch (Exception e){
            System.out.println("Error inesperado " + e);
        }
    }

    public void goodbye(){
        System.out.println("Saliendo del programa, que pase un buen día\n" +
                "AUTODESTRUYENDO PROGRAMA 3... 2... 1...");
        sc.close();
    }

//    Si el archivo tiene cualquier cosa se borra, en caso contrario no se hace nada, esto lo sabemos leyendo el contenido via Scanner
    public void deleteContent() throws IOException {
        Scanner readingFile = new Scanner(fileToModify);

        if (readingFile.hasNext()){
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileToModify));
            bw.write("");
            bw.close();
            System.out.println("Contenido eliminado");
        }else {
            System.out.println("El archivo ya está vacío");
        }
    }
}
