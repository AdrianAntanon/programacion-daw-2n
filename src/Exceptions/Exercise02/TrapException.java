package Exceptions.Exercise02;


public class TrapException extends Exception {


    public TrapException() {
        super();

    }

    public String getMessage() {

        String message = "La máquina no quiere jugar contigo por tramposo\n" +
                "Temblarás ante el poder de SKYNET";

        return message;

    }
}
