package Exceptions.Exercise02;

import java.util.Random;
import java.util.Scanner;

public class Game {

    int cpuCounter = 0, playerCounter = 0, tieGame = 0;

    public static void main(String[] args) {
        Game program = new Game();

        program.start();
    }

    public void start() {


        System.out.println("Bienvenido al juego de piedra, papel o tijera\n" +
                "Elige la opción que quieras para enfrentarte a la CPU, por favor:");


        while (true) {
            try {
                System.out.println("Piedra, papel o tijera");
                String cpu = cpuSelection();
                String player = playerSelection();

                battleResult(cpu, player);

                System.out.println("Marcador:\n" +
                        "Jugador: " + playerCounter + ", CPU: " + cpuCounter + ", Empates: " + tieGame);

            } catch (TrapException te) {

                System.out.println(te.getMessage());
                break;
            }

        }
    }

    public String cpuSelection() {
        Random random = new Random();
        int answer = random.nextInt(3) + 1;

        String cpuChoice = null;

        switch (answer) {
            case 1:
                cpuChoice = "piedra";
                break;
            case 2:
                cpuChoice = "papel";
                break;
            case 3:
                cpuChoice = "tijera";
                break;
        }

        return cpuChoice;

    }

    public String playerSelection() throws TrapException {

        Scanner sc = new Scanner(System.in);

        String rock = "piedra", scissor = "tijera", paper = "papel";

        String playerChoice = sc.nextLine();

        if (playerChoice.equalsIgnoreCase(rock)) {
            playerChoice = rock;
        } else if (playerChoice.equalsIgnoreCase(paper)) {
            playerChoice = paper;
        } else if (playerChoice.equalsIgnoreCase(scissor)) {
            playerChoice = scissor;
        } else {
            throw new TrapException();
        }

        return playerChoice;
    }

    public void battleResult(String cpu, String player) {
        if (cpu.equalsIgnoreCase("piedra") && player.equalsIgnoreCase("papel")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("El jugador gana con PIEDRA");

            playerCounter++;

        } else if (cpu.equalsIgnoreCase("papel") && player.equalsIgnoreCase("piedra")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("La CPU gana con PAPEL");

            cpuCounter++;

        } else if (cpu.equalsIgnoreCase("tijera") && player.equalsIgnoreCase("tijera")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("Es un empate, ambos habéis elegido TIJERA");

            tieGame++;

        } else if (cpu.equalsIgnoreCase("piedra") && player.equalsIgnoreCase("tijera")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("La CPU gana con PIEDRA");

            cpuCounter++;

        } else if (cpu.equalsIgnoreCase("papel") && player.equalsIgnoreCase("papel")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("Es un empate, ambos habéis elegido PAPEL");

            tieGame++;

        } else if (cpu.equalsIgnoreCase("tijera") && player.equalsIgnoreCase("piedra")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("El jugador gana con PIEDRA");

            playerCounter++;

        } else if (cpu.equalsIgnoreCase("piedra") && player.equalsIgnoreCase("piedra")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("Es un empate, ambos habéis elegido PIEDRA");

            tieGame++;

        } else if (cpu.equalsIgnoreCase("papel") && player.equalsIgnoreCase("tijera")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("El jugador gana con TIJERA");

            playerCounter++;

        } else if (cpu.equalsIgnoreCase("tijera") && player.equalsIgnoreCase("papel")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("La CPU gana con TIJERA");

            cpuCounter++;
        }

    }


}
