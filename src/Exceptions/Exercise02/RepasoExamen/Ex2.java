package Exceptions.Exercise02.RepasoExamen;

import Exceptions.Exercise02.Game;
import Exceptions.Exercise02.TrapException;

import java.util.Random;
import java.util.Scanner;

public class Ex2 {
//    Variables globales, contador de cada jugador y empates
    int cpuCounter = 0, playerCounter = 0, tieGame = 0;

    public static void main(String[] args) {
        Ex2 program = new Ex2();

        program.start();
    }

//    Polling infinito y dentro un try catch con excepción personalizada en caso de salir mal
    public void start() {
        System.out.println("Bienvenido al juego de piedra, papel o tijera\n" +
                "Elige la opción que quieras para enfrentarte a la CPU, por favor:");

//  Guardamos la selección de cada participante, mediante funciones, la nuestra la elegimos, la máquina es aleatoria
        while (true) {
            try {
                System.out.println("Piedra, papel o tijera");
                String cpu = cpuSelection();
                String player = playerSelection();

//              Comparamos resultados y procedemos a actualizar el marcador
                battleResult(cpu, player);

                System.out.println("Marcador:\n" +
                        "Jugador: " + playerCounter + ", CPU: " + cpuCounter + ", Empates: " + tieGame);
//              En caso de que escribamos algo que no debamos salta nuestra excepción personalizada
            } catch (Ex2Exception te) {

                System.out.println(te.getMessage());
                break;
            }

        }
    }

    public String cpuSelection() {
        Random random = new Random();
        int answer = random.nextInt(3) + 1;

        String cpuChoice = null;

        switch (answer) {
            case 1:
                cpuChoice = "piedra";
                break;
            case 2:
                cpuChoice = "papel";
                break;
            case 3:
                cpuChoice = "tijera";
                break;
        }

        return cpuChoice;

    }

    public String playerSelection() throws Ex2Exception {
// En caso de introducir algo que no sea una opción viable lanzaremos la Ex2Exception que hemos creado en otra clase
        Scanner sc = new Scanner(System.in);

        String rock = "piedra", scissor = "tijera", paper = "papel";

        String playerChoice = sc.nextLine();

        if (playerChoice.equalsIgnoreCase(rock)) {
            playerChoice = rock;
        } else if (playerChoice.equalsIgnoreCase(paper)) {
            playerChoice = paper;
        } else if (playerChoice.equalsIgnoreCase(scissor)) {
            playerChoice = scissor;
        } else {
            throw new Ex2Exception();
        }

        return playerChoice;
    }

//    Esto simplemente es la forma de comparar, básicamente es un 3x3, 3 victorias, 3 derrotas y 3 empates son las posibilidades
    public void battleResult(String cpu, String player) {
        if (cpu.equalsIgnoreCase("piedra") && player.equalsIgnoreCase("papel")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("El jugador gana con PIEDRA");

            playerCounter++;

        } else if (cpu.equalsIgnoreCase("papel") && player.equalsIgnoreCase("piedra")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("La CPU gana con PAPEL");

            cpuCounter++;

        } else if (cpu.equalsIgnoreCase("tijera") && player.equalsIgnoreCase("tijera")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("Es un empate, ambos habéis elegido TIJERA");

            tieGame++;

        } else if (cpu.equalsIgnoreCase("piedra") && player.equalsIgnoreCase("tijera")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("La CPU gana con PIEDRA");

            cpuCounter++;

        } else if (cpu.equalsIgnoreCase("papel") && player.equalsIgnoreCase("papel")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("Es un empate, ambos habéis elegido PAPEL");

            tieGame++;

        } else if (cpu.equalsIgnoreCase("tijera") && player.equalsIgnoreCase("piedra")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("El jugador gana con PIEDRA");

            playerCounter++;

        } else if (cpu.equalsIgnoreCase("piedra") && player.equalsIgnoreCase("piedra")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("Es un empate, ambos habéis elegido PIEDRA");

            tieGame++;

        } else if (cpu.equalsIgnoreCase("papel") && player.equalsIgnoreCase("tijera")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("El jugador gana con TIJERA");

            playerCounter++;

        } else if (cpu.equalsIgnoreCase("tijera") && player.equalsIgnoreCase("papel")) {

            System.out.println("El jugador ha elegido: " + player + ", la CPU ha elegido: " + cpu);

            System.out.println("La CPU gana con TIJERA");

            cpuCounter++;
        }

    }
}
