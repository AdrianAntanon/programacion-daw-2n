package Exceptions.practiseOne;

class RGBColor {
    private int red, green, blue;

    public RGBColor(int red, int green, int blue) throws RGBColorException {
        if (red < 0 || red > 255) {
            throw new RGBColorException("red",red);
        } else if (green < 0 || green > 255) {
            throw new RGBColorException("green",green);
        } else if (blue < 0 || blue > 255) {
            throw new RGBColorException("blue",blue);
        } else {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

    }

    public void colorAssign(int red, int green, int blue) throws RGBColorException {
        if (red < 0 || red > 255) {
            throw new RGBColorException("red",red);
        } else if (green < 0 || green > 255) {
            throw new RGBColorException("green",green);
        } else if (blue < 0 || blue > 255) {
            throw new RGBColorException("blue",blue);
        } else {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }
    }


    public void redColorAssign(int red) throws RGBColorException {
        if (red < 0 || red > 255) {
            throw new RGBColorException("red",red);
        } else {
            this.red = red;
        }
    }

    public void greenColorAssign(int green) throws RGBColorException {
        if (green < 0 || green > 255) {
            throw new RGBColorException("green",green);
        } else {
            this.green = green;
        }
    }

    public void blueColorAssign(int blue) throws RGBColorException {
        if (blue < 0 || blue > 255) {
            throw new RGBColorException("blue",blue);
        } else {
            this.blue = blue;
        }

    }

    public int[] colorObtain() {
        int [] color = {red, green, blue};
        return color;
    }

    public int redColorObtain() {
        return red;
    }

    public int greenColorObtain() {
        return green;
    }

    public int blueColorObtain() {
        return blue;
    }
}
