package Exceptions.practiseOne;

class Pixel {

    private int x;
    private int y;
    private RGBColor color;

    public Pixel(int x, int y, int red, int green, int blue) throws RGBColorException{
        this.x = x;
        this.y = y;

        if (red < 0 || red > 255) {
            throw new RGBColorException("red",red);
        } else if (green < 0 || green > 255) {
            throw new RGBColorException("green",green);
        } else if (blue < 0 || blue > 255) {
            throw new RGBColorException("blue",blue);
        } else {
            color = new RGBColor(red, green, blue);
        }
    }

    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setXPosition(int x){
        this.x = x;
    }

    public void setYPosition(int y){
        this.y = y;
    }

    public int[] getPosition(){
        int[] position = {x, y};
        return position;
    }

    public int geXPosition(){
        return x;
    }

    public int getYPosition(){
        return y;
    }

    public void setColor(int red, int green, int blue) throws RGBColorException {
        color.colorAssign(red, green, blue);
    }

    public void setRedColor(int red) throws RGBColorException {
        color.redColorAssign(red);
    }

    public void setGreenColor(int green) throws RGBColorException {
        color.greenColorAssign(green);
    }

    public void setBlueColor(int blue) throws RGBColorException {
        color.blueColorAssign(blue);
    }


    public int[] getColor(){
        return color.colorObtain();
    }

    public int getRedColor(){
        return color.redColorObtain();
    }

    public int getGreenColor(){
        return color.greenColorObtain();
    }

    public int getBlueColor(){
        return color.blueColorObtain();
    }





}
