package Exceptions.practiseOne;

public class RGBColorException extends Exception {

    private String errorCode;
    private int color;

    public RGBColorException(String errorCode, int color){
        super();
        this.errorCode = errorCode;
        this.color = color;
    }

    @Override
    public String getMessage() {

        String message = "";

        switch (errorCode){
            case "vermell":
            case "rojo":
            case "red":
                message = "El color rojo no puede ser " + color + ", debe estar comprendido entre 0 y 255";
                break;
            case "verde":
            case "verd":
            case "green":
                message = "El color verde no puede ser " + color + ", debe estar comprendido entre 0 y 255";
                break;
            case "azul":
            case "blau":
            case "blue":
                message = "El color azul no puede ser " + color + ", debe estar comprendido entre 0 y 255";
                break;
        }

        return message;
    }
}
