package Exceptions.practiseOne;

public class ExceptionPropagation {

    public static void main(String[] args){
        try {
            RGBColor blueColor = new RGBColor(15, 22, 200);
            int[] color = blueColor.colorObtain();
            System.out.print("El color RGB elegido está formado por los valores: ");
            for (int singleColor : color) {
                System.out.print(singleColor + " ");
            }

            blueColor.colorAssign(133, 255, 133);

            color = blueColor.colorObtain();

            System.out.print("\n" +
                    "Ahora los colores están cambiados: ");
            for (int singleColor : color) {
                System.out.print(singleColor + " ");
            }

            /*
            * Si quiero que falle hago lo siguiente
            * blueColor.redColorAssign(256);
            * blueCOlor.greenColorAssign(-10);
            * */

            System.out.print("\n" +
                    "Si quiero cambiar solo un color también puedo: ");
            blueColor.redColorAssign(255);

            color = blueColor.colorObtain();

            for (int singleColor : color) {
                System.out.print(singleColor + " ");
            }


            Pixel pixel = new Pixel(12, 20, 150, 120, 12);

            System.out.print("\n" +
                    "Con la clase Pixel también podemos usar más métodos, como por ejemplo obtener la posición X e Y ");
            int [] positions = pixel.getPosition();
            for (int position : positions) {
                System.out.print(position + " ");
            }

            System.out.print("\n" +
                    "Además de seguir pudiendo acceder al color ");
            color = pixel.getColor();
            for (int singleColor : color) {
                System.out.print(singleColor + " ");
            }

            System.out.print("\n" +
                    "Y podemos cambiar lo que queramos sin problemas, como el azul ");
            pixel.setBlueColor(255);
            System.out.println(pixel.getBlueColor());

            /*
            * Y si queremos comprobar que falla hacemos lo siguiente
            * pixel.setRedColor(1000);
            *
            * */

        }catch (RGBColorException e){
            System.out.println(e.getMessage());
        }

    }
}
